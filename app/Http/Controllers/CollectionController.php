<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Rules\Uppercase;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\LazyCollection;

class CollectionController extends Controller
{
    public function create()
    {
        $collection = collect([1,2,3,4,5,6]);
        dd($collection->toArray());
        return $collection;
    }

    public function macro()
    {
        Collection::macro('toUpper' , function() {
            return $this->map(function($value) {
                return Str::upper($value);
            });
        });

        $collection = collect(['milad' , 'ali']);
        $uppper = $collection->toUpper();
        dd($uppper);
    }

    public function methods()
    {
        $collection = Post::all();
        dd($collection->all());
    }

    public function ave()
    {
        // $age = collect([
        //     'age' => 14,
        //     'age' => 25,
        //     'age' => 30,
        //     'age' => 19,
        //     'age' => 27
        // ])->ave('age');
        // dd($age);

        $collection = collect([1,2,3,4])->ave();
        return $collection;
        dd($collection);
    }

    public function chunkMeth()
    {
        $collection = collect([1,2,3,4,5,6,7,8,9]);
        $chunks = $collection->chunk(3);
        return $chunks->all();
    }

    public function chunkWhileMeth()
    {
        $collection = collect(str_split('AABBCCCD'));

        $chunks = $collection->chunkWhile(function ($value, $key, $chunk) {
            return $value === $chunk->last();
        });

        return $chunks->all();
    }

    public function collapseMeth()
    {
        $collection = collect([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ]);
        
        $collapsed = $collection->collapse();
        
        return $collapsed->all();
    }

    public function collectMeth()
    {
        $lazyCollection = LazyCollection::make(function () {
            yield 1;
            yield 2;
            yield 3;
        });
        
        $collection = $lazyCollection->collect();
        
        get_class($collection);

        return $collection->all();
    }

    public function combineMeth()
    {
        $collection = collect(['name', 'age']);

        $combined = $collection->combine(['George', 29]);

        return $combined->all();
    }

    public function contactMeth()
    {
        $collection = collect(['milad', 'ghadir']);

        $concatenated = $collection->concat(['nima', 'reza'])->concat(['name' => 'ali' , 'hasan']);

        return $concatenated->all();
    }

    public function containsMethI()
    {
        $collection = collect([1, 2, 3, 4, 5]);

        $collection->contains(function ($value, $key) {
            dump($value*$value);
            return $value > 5;
        });
    }

    public function containsMethII()
    {
        $collection = collect(['name' => 'Desk', 'price' => 100]);

        $exe1 = $collection->contains('Desk');

        $exe2 = $collection->contains('chair');
        dump($exe1 , $exe2);
    }

    public function countMeth()
    {
        $num = collect([1,1,2,2,5,5,6,4,8,5,7,4,5]);
        dump($num->count());
    }

    public function countByMeth()
    {
        $num = collect([1,1,2,2,5,5,6,4,8,5,7,4,5,1]);
        return $num->countBy();
    }

    public function crossJoinMeth()
    {
        $test = 'test';
        $collection = collect([1, 2]);

        $matrix = $collection->crossJoin(['a', 'b'] , ['I' , 'V']);

        $test = $matrix->all();

        return $test;
    }

    public function diffMeth()
    {
        $collection = collect([1,2,3,4,5,6]);
        $diff = $collection->diff([2,4,6,8]);
        return $diff->all();
    }

    public function eachMeth()
    {
        $posts = Post::all();
        $posts->each(function ($post , $key)
        {
            if ($key == 2) {
                return false;
            }
            dump($post->post);
        });
    }

    public function everyMeth()
    {
        collect([1, 2, 3, 4])->every(function ($value, $key) {
            if ($value > 0) {
                dump('true');
            }
            else {
                dump('false');
            }
            // return $value > 2;
        });
    }

    public function filterMeth()
    {
        $collection = collect([1, 2, 3, 4]);

        $filtered = $collection->filter(function ($value, $key) {
            return $value > 2;
        });

        return $filtered->all();
    }

    public function rejectMeth()
    {
        $collection = collect([1, 2, 3, 4]);

        $reject = $collection->reject(function ($value, $key) {
            return $value > 2;
        });

        return $reject->all();
    }

    public function firstMeth()
    {
        $collection = collect(['8', 2, 3, 4]);        

        return $collection->first();
    }

    public function firstWhereMeth()
    {
        $collection = collect([
            ['name' => 'Regena', 'age' => null],
            ['name' => 'Linda', 'age' => 14],
            ['name' => 'Diego', 'age' => 23],
            ['name' => 'Linda', 'age' => 84],
        ]);
        
        return $collection->firstWhere('age');
    }

    public function groupbyMeth()
    {
        $collection = collect([
            ['account_id' => 'account-x10', 'product' => 'Chair'],
            ['account_id' => 'account-x10', 'product' => 'Chair'],
            ['account_id' => 'account-x11', 'product' => 'Desk'],
        ]);
        
        $grouped = $collection->groupBy('account_id');
        
        $result = $grouped->all();

        dd($result);
    }

    public function mapMeth()
    {
        $collection = collect([
            'name' => 'milad',
            'fanily' => 'mohammadi',
            'job' => 'backend developer'
        ]);

        $newCollection = $collection->map(function($item) {
            return strtoupper($item);
        })->flip();

        $v1 = $newCollection->map(function($item) {
            return strtoupper($item);
        });
        
        dd($v1);
    }

    public function partitionMeth()
    {
        $collection = collect([1, 2, 3, 4, 5, 6,7,8,9,10]);

        [$underThree, $equalOrAboveThree] = $collection->partition(function ($i) {
            return $i < 3;
        });

        $exe1 = $underThree->all();

        $exe2 = $equalOrAboveThree->all();
        dump($exe1 , $exe2);
    }

    public function pipeMeth()
    {
        $collection = collect([1, 2, 3,4,5,6,7,8,9,10]);

        $piped = $collection->pipe(function ($collection) {
            return $collection->sum();
        });

        dd($piped);
    }

    public function pluckMeth()
    {
        $collection = collect([
            ['product_id' => 'prod-100', 'name' => 'Desk'],
            ['product_id' => 'prod-200', 'name' => 'Chair'],
        ]);

        $plucked1 = $collection->pluck('name');
        $plucked2 = $collection->pluck('product_id' , 'name');

        dump($plucked1 , $plucked2);
    }

    public function popMeth()
    {
        $collection = collect([1, 2, 3, 4, 5]);

        $popitem = $collection->pop();

        $result = $collection->all();

        dump($popitem , $result);
    }

    public function prependMeth()
    {
        $collection = collect([1, 2, 3, 4, 5]);

        $collection->prepend(0);

        $prepend = $collection->all();

        dump($prepend);
    }

    public function prependIMeth()
    {
        $collection = collect(['one' => 1, 'two' => 2]);

        $collection->prepend(0, 'zero');

        $prepend = $collection->all();

        dump($prepend);
    }

    public function pullMeth()
    {
        $collection = collect(['product_id' => 'prod-100', 'name' => 'Desk']);

        $pull = $collection->pull('name');

        $result = $collection->all();

        dump($pull , $result);
    }

    public function pushMeth()
    {
        $collection = collect([1, 2, 3, 4]);

        $push = $collection->push(5);

        $result = $collection->all();

        dump($push , $result);
    }

    public function putMeth()
    {
        $collection = collect(['product_id' => 1, 'name' => 'Desk']);

        $put = $collection->put('price', 100);

        $result = $collection->all();

        dump($put , $result);
    }

    public function randomMeth()
    {
        $collection = collect([1, 2, 3, 4, 5]);

        $result = $collection->random();

        dump($result);
    }
    
    public function randomIMeth()
    {
        $collection = collect([1, 2, 3, 4, 5]);

        $result = $collection->random(3);

        dump($result);
    }

    public function replaceMeth()
    {
        $collection = collect(['Taylor', 'Abigail', 'James']);

        $replaced = $collection->replace([1 => 'Victoria', 3 => 'Finn']);

        $result = $replaced->all();

        dump($result);
    }

    public function replaceRecursiveMeth()
    {
        $collection = collect([
            'Taylor',
            'Abigail',
            [
                'James',
                'Victoria',
                'Finn'
            ]
        ]);
        
        $replaced = $collection->replaceRecursive([
            'Charlie',
            2 => [1 => 'King']
        ]);
        
        $result = $replaced->all();

        dump($result);
    }

    public function reverseMeth()
    {
        $collection = collect(['a', 'b', 'c', 'd', 'e']);

        $reversed = $collection->reverse();

        $result = $reversed->all();

        dump($result , $reversed);
    }

    public function searchMeth()
    {
        $collection = collect([2, 4, 6, 8]);

        $result = $collection->search(8);

        $result2 = collect([2, 4, 6, 8])->search('4', $strict = true);

        $result3 = collect([2, 4, 6, 8])->search(function ($item, $key) {
            return $item > 5;
        });

        dump($result , $result2 , $result3);
    }

    public function shiftMeth()
    {
        $collection = collect([1, 2, 3, 4, 5]);

        $collection->shift();

        $result = $collection->all();

        dump($result);
    }

    public function shuffleMeth()
    {
        // $collection = collect([1, 2, 3, 4, 5]);
        $collection = collect(['milad' , 'ali' , 'saeed' , 'amir']);

        $shuffled = $collection->shuffle();

        $result = $shuffled->all();

        dump($result);
    }

    public function slidingIMeth()
    {
        $collection = collect([1, 2, 3, 4, 5]);

        $chunks = $collection->sliding(3);
        
        $result = $chunks->toArray();

        dump($result);
    }

    public function slidingIIMeth()
    {
        $collection = collect([1, 2, 3, 4, 5 ,6 ,7 ,8 ,9]);
        $chunks = $collection->sliding(3 ,  2);
        $result = $chunks->toArray();
        dump($result);        
    }

    public function skipMeth()
    {
        $collection = collect([11, 12, 13, 14, 15, 16, 17, 18, 19, 20]);
        $collection = $collection->skip(3);
        $result = $collection->all();
        dump($result);
    }

    public function skipUntilMeth()
    {
        $collection = collect([11, 12, 13, 14, 15, 16, 17, 18, 19, 20 , 9]);
        $subset = $collection->skipUntil(function ($item) {
            return $item >= 16;
        });

        $result = $subset->all();
        dump($result);
    }

    public function skipWhileMeth()
    {
        $collection = collect([1,2,8,9,6,5,5,12,20]);
        $subset = $collection->skipWhile(function ($item) {
            return $item <= 8;
        });

        $result = $subset->all();
        dump($result);
    }

    public function sliceMeth()
    {
        $collection = collect([11, 12, 13, 14, 15, 16, 17, 18, 19, 20 , 9]);
        $slice1 = $collection->slice(2);
        $slice2 = $collection->slice(4, 2);
        $result1 = $slice1->all();
        $result2 = $slice2->all();
        dump($result1 , $result2);
    }

    public function soleMeth()
    {
        $result1 = collect([1, 2, 3, 4])->sole(function ($value, $key) {
            return $value === 2;
        });

        $collection = collect([
            ['product' => 'Desk', 'price' => 200],
            ['product' => 'Chair', 'price' => 100],
        ]);        
        $result2 = $collection->sole('product', 'Chair');

        $collection = collect([
            ['product' => 'Desk', 'price' => 200],
        ]);
        
        $result3 = $collection->sole();
        dump($result1 , $result2, $result3);
    }

    public function sortMeth()
    {
        $collection = collect([5, 3, 1, 2, 4]);
        $sorted = $collection->sort();
        dump($sorted);
    }

    public function sortByMeth()
    {
        $collection = collect([
            ['name' => 'Desk', 'price' => 200],
            ['name' => 'Chair', 'price' => 100],
            ['name' => 'Bookcase', 'price' => 150],
        ]);        
        $sorted = $collection->sortBy('price');        
        $result = $sorted->values()->all();

        dump($sorted , $result);
    }

}
