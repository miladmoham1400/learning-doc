<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostStoreRequest;
use App\Models\Comment;
use Illuminate\Support\Facades\Log;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public $users;
    public function __construct()
    {
        $users = User::all();
        $this->users = $users; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $users)
    {
        $comments = Comment::all();
        return view('comments.index' , compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd('create');
        return view('comments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'user_id' => ['required' , 'exists:users,id'],
            'post_id' => ['required' , 'exists:posts,id'],
            'title' => ['required'],
            'comment' => ['required']
        ]);
        dd($validated);
        $comment = Comment::create([
            'user_id' => auth()->user()->id,
            'post_id' => 2,
            'title' => $request['title'],
            'comment' => $request['comment']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        // $comment = Comment::find($this->route('comments.show'));
        // dd($comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        return view('comments.edit' , compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(PostStoreRequest $request, Comment $comment)
    {
        // dd($request->toArray());
        $comment->update([
            'user_id' => auth()->user()->id,
            'post_id' => 2,
            'title' => $request['title'],
            'comment' => $request['comment']
        ]);
        return redirect('/comments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
