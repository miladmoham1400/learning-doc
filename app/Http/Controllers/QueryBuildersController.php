<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QueryBuildersController extends Controller
{
    public function DBQ()
    {
        $posts = DB::table('posts')->get();
        return $posts;
    }

    public function pluckQ()
    {
        $slugs = DB::table('posts')->pluck('slug' , 'post');
        return $slugs;
    }
}
