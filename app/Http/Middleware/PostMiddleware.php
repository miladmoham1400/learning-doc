<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PostMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next ,$role)
    {
        if ($role == 'admin') {
            dump('post middleware');
        }
        else {
            dump('you are '.$role);
        }
        return $next($request);
    }
}
