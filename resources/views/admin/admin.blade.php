@extends('layouts.app')



@section('content')

    <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <div class="col-6">
                <div class="alert alert-danger" role="alert">
                    {{ url()->current() }}
                </div>
                <div class="alert alert-danger" role="alert">
                    {{ url()->full() }}
                </div>
                <div class="alert alert-danger" role="alert">
                    {{ url()->previous() }}
                </div>
                <div class="alert alert-danger" role="alert">
                    {{ URL::current() }}
                </div>
    
                <button type="button" class="btn btn-primary">Primary</button>
            </div>
        </div>
    </div>

@endsection