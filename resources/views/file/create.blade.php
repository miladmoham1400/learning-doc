@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{ route('files.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group mb-0 mt-3">
                    <label for="title">Title :</label>
                    <input class="form-control @error('title') is-invalid @enderror" type="text" id="title" name="title" value="{{ old('title') }}">
                </div>
                @error('title')
                    <p class="text text-danger mb-1 mt-0">{{ $message }}</p>
                @enderror


                <div class="form-group mb-0 mt-3">
                    <label for="photo">File :</label>
                    <input class="form-control" type="file" name="photo" id="photo" value="{{ old('photo') }}">
                </div>
                @error('file')
                    <p class="text text-danger mb-1 mt-0">{{ $message }}</p>
                @enderror


                <div class="form-group mt-3">
                    <input class="form-control btn btn-success" type="submit" value="Create">
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
