@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="d-flex justify-content-between mb-3">
                <h3>Comments Lists :</h3>
                <a href="{{ route('comments.create') }}" class="btn btn-success btn">New Comment</a>
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>title</th>
                        <th>Photo</th>
                        <th>option</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($files as $file)
                        <tr>
                            <td>{{ $file->id }}</td>
                            <td>{{ $file->title }}</td>
                            <td>{{ $file->photo }}</td>
                            <td>
                                <a href="{{ route('files.edit' , $file) }}" class="btn btn-warning btn-sm">edit</a>
                                <a href="" class="btn btn-danger btn-sm">delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
