<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
    <div>
        <form action="/user/store" method="POST">
            @csrf
            <label for="fname">Name :</label>
            <input type="text" id="fname" name="fname">
            <br><br>
            <label for="lname">Last Name :</label>
            <input type="text" id="lname" name="lname">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" name="rememberme" id="my-checkbox">
                <label class="form-check-label" for="my-checkbox">
                    remember :
                </label>
            </div>
            <input type="submit" value="send">
        </form>
    </div>
</body>
</html>