<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>profile</title>
</head>
<body>
    <div>
        <p><strong>name :</strong>{{ $name }}</p>
        <p><strong>email :</strong>{{ $email }}</p>
        <p><strong>company :</strong>{{ $company }}</p>
    </div>
</body>
</html>